def check_even(num):
    if(num % 2 == 0):
        return num


#print("Even number from the range of 1-10 are: {}".format(list(filter(check_even, range(10)))))
print(f"Even number from the range of 1-10 are: {list(filter(check_even, range(10)))}")
print("Execution is successful")
print("Another change to test workspace")
